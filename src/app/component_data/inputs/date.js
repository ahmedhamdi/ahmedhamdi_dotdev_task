import React, { Component } from 'react';
import moment from 'moment';
import { DatePicker } from 'antd';

export class date extends Component {
    render() {
        const {input:{value,...input},type,label,placeholder, format="HH:mm:ss" }=this.props

        return (
           <DatePicker {...input} value={value?moment(value,format):undefined} placeholder={placeholder ||label} type={type}/>
        );
    }
}
