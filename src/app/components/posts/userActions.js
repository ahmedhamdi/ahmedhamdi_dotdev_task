import React, { Component } from 'react';
import {Button,Modal, Icon} from "antd";
import Pop_up from "./pop_up";
import {mapStateToProps,mapDispatchToProps} from "./mapping";
import { connect } from 'react-redux';
import {withRouter} from "react-router";
class UserActions extends Component {
handel_pop_up(){
    this.props.setcurrentdata(this.props.post_item)
    this.props.history.push("/details")
}
handle_like(){
    this.props.like(this.props.post_item.id)
}
render() {
    return(
        <div>
            <Button onClick={this.handle_like.bind(this)}>
                <Icon type="like" theme={this.props.post_item.like ? "twoTone":"filled"}/>
            </Button>
           
            <Button icon="search" onClick={this.handel_pop_up.bind(this)}/>

        </div>

    );
}
}
export default connect(null,mapDispatchToProps)( withRouter(UserActions));
