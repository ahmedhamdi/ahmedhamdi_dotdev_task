export const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        setdata: (data) => {
            dispatch({
                type: "set_data",
                data
            })
        },
        setcurrentdata:(currentdata)=>{
            dispatch({
                type:"set_current_data",
                currentdata
            })

        },
        like(id){
            dispatch({
                type: "update_post_like",
                id
            })
        }

    }
}
export const mapStateToProps = ({posts}) => {
    return {
        data:posts.data,
        currentdata:posts.currentdata
}
}
