import { Modal, Button } from 'antd';
import React, { Component } from 'react';
import {mapStateToProps} from "./mapping";
import { connect } from 'react-redux';


class Pop_up extends Component{
  constructor(props){
    super(props);
    this.state = {
      visible: false,
        }

  }

  
  componentWillMount(){
      this.setState({
          visible:true
      })
  }

  handleaction = (e) => {
    console.log(e);
    this.setState({
      visible: false,
    });
    this.props.history.replace("/");

  }


  render() {
    return (
      <div>
       
        <Modal
          title="More info"
          visible={this.state.visible}
          closable={false}
          onOk={this.handleaction}
          onCancel={this.handleaction}
        >
          <p>{this.props.currentdata.title}</p>
         
        </Modal>
      </div>
    );
  }
}

export default connect(mapStateToProps,null)(Pop_up);
