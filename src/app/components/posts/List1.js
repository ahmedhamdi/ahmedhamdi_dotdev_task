import React, { Component } from 'react';
import {mapStateToProps,mapDispatchToProps} from "./mapping";
import { connect } from 'react-redux';
import { Card, Button, Avatar } from 'antd';
import UserActions from "./userActions";

const { Meta } = Card;

class List1 extends Component {
 
    render(){
       
        const {data}=this.props;
        return(
                    <div>
                  <p>list 1</p>
                   { data.map((d,v)=>(
                       <div key={v}>
                           <Card
                           style={{ width: 200 }}
                          cover={<img alt="example" src="https://picsum.photos/200/150" />}
                          >
                         <Meta
                         title={d.userId}
                        description="This is the description"
                            />
                            </Card>
                             <UserActions post_item={d}/>
                            </div>

                       ))} 
             </div>

        )
    }
}
export default connect(mapStateToProps,mapDispatchToProps)(List1);
