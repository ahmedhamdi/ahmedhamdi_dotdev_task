import React, { Component } from 'react';
import axios from 'axios';
import {Button} from "antd";
import {uniqBy} from 'lodash';
import {mapDispatchToProps} from "./mapping";
import { connect } from 'react-redux';
import List1 from "./List1";
import List2 from "./List2";
import './style.css';

 class Post extends Component {
     constructor(props){
         super(props)
         this.state={
            
             temp:false
         }
     }
     get_data(){
        axios.get("https://jsonplaceholder.typicode.com/posts")
        .then( (response)=> {
          // handle success
        let uniq_data = uniqBy(response.data, 'userId');
        this.props.setdata(uniq_data)
          this.setState({
              temp:!this.state.temp
          })
        })
        .catch((error)=> {
          // handle error
          console.log(error);
        })
    }

     render() {
        const {temp}=this.state;
         return (
             <div className="show-posts">
                   {temp==false&&<Button type="primary" onClick={this.get_data.bind(this)}> Get posts </Button> } 
                   <section className="two-list-container">
                        {temp&&  <List1/>}
                        {temp&&  <List2 />}

                   </section>

             </div>
         );
     }
 }
 
 export default connect(null,mapDispatchToProps)(Post);
  