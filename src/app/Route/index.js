import React, { Component } from 'react';
import { HashRouter as Router, Route, Switch } from 'react-router-dom';
import Header from "../components/Header"
import Posts from '../components/posts'
import Pop_up from '../components/posts/pop_up';
class RouteComponent extends Component {
    render() {
        return (
            <Router>
                <section>
                    <Header />
                    <Posts/>
                    <Switch>
                        <Route path="/details" component={Pop_up}/>
                    </Switch>
                    {/* <Footer/> */}
                </section>
              </Router>
        );
    }
}

export default RouteComponent;