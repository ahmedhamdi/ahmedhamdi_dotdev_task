
export const set_data = (state={}, payload) => {
    return {

        ...state,data:payload.data
    };
}
export const set_current_data =(state={},payload)=>{
    return{
        ...state,currentdata:payload.currentdata
    };
}


export const update_post_like =(state={},payload)=>{
    return{
        ...state,data:state.data.map(d=>(d.id == payload.id ? {...d, like: !d.like}:d))
    };
}