import * as handlers from './handler'

export const posts = (state = {}, action) => {
    return handlers[action.type]
        ? handlers[action.type](state, action)
        : state;
};