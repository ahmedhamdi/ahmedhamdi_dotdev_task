var path = require('path');
var fs = require('fs');
const lessToJs = require("less-vars-to-js");
const themeVariables = lessToJs(fs.readFileSync(path.join(__dirname, "./theme.less"), "utf8"));


module.exports = {
    entry: {
        app_vendors: [
            "react", "react-dom"
        ],
        common: './src/index.js'
    },
    devtool: "inline-module-source-map",
    watch: true,
    output: {
        filename: '[name].min.js',
        chunkFilename: '[id].[hash].chunk.js',
        umdNamedDefine: true,
        publicPath: "js/dist/",
        path: path.resolve(__dirname, 'www/js/dist')
    },
    node: {
        fs: 'empty'
    },
    module: {
        loaders: [
            {
                test: /antd.*\.less$/,
                use: [
                    {
                        loader: "style-loader"
                    }, {
                        loader: "css-loader"
                    }, {
                        loader: "less-loader",
                        options: {
                            javascriptEnabled: true,
                            modifyVars: themeVariables
                        }
                    }
                ]
            }, {
                test: /\.less$/,
                exclude: /(node_modules|bower_components)/,
                use: [
                    {
                        loader: "style-loader"
                    }, {
                        loader: "css-loader",
                        options: {
                            module: true
                        }
                    }, {
                        loader: "less-loader",
                        options: {
                            javascriptEnabled: true,
                            modifyVars: themeVariables
                        }
                    }
                ]
            }, {
                test: /\.css$/,
                use: [
                    {
                        loader: "style-loader"
                    }, {
                        loader: "css-loader"
                    }
                ]
            }, {
                test: /\.es6$/,
                exclude: /node_modules/,
                loader: 'babel-loader',
                query: {
                    presets: ["es2015", "stage-0", "react"]
                }
            }, {
                test: /\.jsx/,
                loader: 'babel-loader',
                exclude: /(node_modules|bower_components)/,
                include: path.join(__dirname, 'src')
            }, {
                test: /\.js$/,
                include: path.join(__dirname, 'src'),
                exclude: /(node_modules|bower_components)/,
                loader: 'babel-loader',
                // query: {    presets: ['es2015'] }
            }, {
                test: /\.html$/,
                loader: "html-loader"
            }
        ]
    },
    resolve: {
        modules: [
            path.resolve('./src'),
            path.resolve('./src/imports'),
            path.resolve('./node_modules')
        ],
        extensions: ['.js', '.es6', '.jsx']
    },
    plugins: [
        //  "transform-runtime",  ["transform-es2015-modules-commonjs-simple", {
        // "noMangle": true  }], new webpack.IgnorePlugin(/^\.\/locale$/, [/moment$/]),
        // new webpack.NoErrorsPlugin(),   new CompressionPlugin({     asset:
        // "[path].gz[query]",     algorithm: "gzip",     test: /\.js$|\.css$|\.html$/,
        //    threshold: 10240,     minRatio: 0   }),   new
        // webpack.optimize.CommonsChunkPlugin({      // The order of this array matters
        //      names: ["common", "vendor"],      minChunks: Infinity  }),   new
        // webpack.DefinePlugin({ // <-- key to reducing React's size     'process.env':
        // {       'NODE_ENV': JSON.stringify('production')     }   }), new
        // webpack.optimize.DedupePlugin(), //dedupe similar code new
        // webpack.optimize.UglifyJsPlugin(), //minify everything   new
        // webpack.optimize.AggressiveMergingPlugin(),//Merge chunks     new
        // BundleAnalyzerPlugin(),
        //
        //     // new webpack.NoErrorsPlugin(),
    ]
}
